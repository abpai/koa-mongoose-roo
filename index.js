"use strict";

 /**
 * Module Dependencies
 */

var debug = require('debug')('krm');
var roo = require('roo')(__dirname);
var port = process.env.PORT || 3002;
var parser = require('koa-bodyparser');
var error = require('koa-error');
var cors = require('koa-cors');

var production = process.env.NODE_ENV == 'production' ? true : false;

/**
 * Export the koa app for testing
 */

module.exports = roo.app;

if (production) roo.app.proxy = true;

/**
 * Middleware
 */
roo.use(error());
roo.use(cors());

// Do not log static files GET's in production
roo.logger(function(ctx) {
  return true//production && 'GET' == ctx.method && extname(ctx.url) ? false : true;
});

roo.static('build');
roo.static('public', {
  maxage: 1000*60*60*24
});

roo.use(parser());

/**
 * Routing
 */

roo.use(require('./lib/middleware-redirect-error')());
roo.use(require('./lib/middleware-locals'));

// support 404
roo.mount('/404', require('./pages/404'));

// API
roo.mount('/api', require('./lib/api'));
roo.mount('/ie', require('./pages/ie'));
roo.mount('/', require('./pages/home'));


/**
 * Handle uncaught exceptions
 *
 * @todo: signal to restart
 */

process.on('uncaughtException', function(err) {
  console.error(err.stack);
});

/**
 * Listen
 */

roo.listen(port, function() {
  console.log('listening on: localhost:%s', this.address().port);
});

/**
 * Handle errors coming from koa
 *
 * @todo: signal to restart
 */
roo.app.on('error', function(err) {
  console.error(err.stack);
});
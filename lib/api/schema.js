var mongoose = require('mongoose');

var Schema = mongoose.Schema;
var rawModelSchema = new Schema({
  data: Schema.Types.Mixed,
  createdAt: { type: Date, default: Date.now }
})

module.exports = mongoose.model('Raw', rawModelSchema);

"use strict";

/**
 * Module Dependencies
 */

var debug = require('debug')('krm:api');
var Roo = require('roo');
var mongoose = require('mongoose');
var verify = require('./../middleware-verify');

var API_TOKEN = 'bluebirds';
var VERIFY = true;


/*
Mongo
*/

mongoose.connect(process.env.MONGO_URL);
var Raw = require('./schema.js');

/**
 * Initialize roo
 */

var roo = module.exports = Roo(__dirname);

roo.use(function *(next) {
  try {
    yield next;
  } catch (err) {
    this.status = err.status || 500;
    this.body = err.message;
    this.app.emit('error', err, this);
  }
});


function *logger(next) {
  console.log('request came in');
  yield next;
}

roo
  .get('/', verify(VERIFY, API_TOKEN), function *(next) {
    if(this.locals.valid) {
      this.body = {
        success: "Welcome Lapwing",
        routes: [
          ".get('/')",
          ".get('/search')",
          ".post('/')",
          ".delete('/:id')",
          ".put('/:id')"
        ]
      };
    } else {
      this.body = {error: "Please provide a valid API Token"};
    }
  })

  .get('/search', verify(VERIFY, API_TOKEN), function *(next) {
    if(this.locals.valid) {
      try {
        var data = yield Raw.find(this.request.body)
                            .sort({createdAt: -1})
                            .exec()
        this.body = {
          success: true,
          length: data.length,
          data: data
        };
      } catch (e) {
        this.body = {error: String(e)};
      }
    } else {
      this.body = {error: "Please provide a valid API Token"};
    }
  })

  .post('/', verify(VERIFY, API_TOKEN), function *(next) {
    if(this.locals.valid) {
      try {
        var data = new Raw({"data": this.request.body});
        yield data.save();
        this.body = {
          success: true,
          created: data
        };
      } catch (e) {
        this.body = {error: String(e)};
      }
    } else {
      this.body = {error: "Please provide a valid API Token"};
    }
  })
  
  .delete('/:id', verify(VERIFY, API_TOKEN), function *(next) {
    if(this.locals.valid) {
      try {
        var data = yield data.findOneAndRemove({
                                        _id:this.params.id
                                      }).exec();
        this.body = {
          success: true,
          deleted: data
        };
      } catch (e) {
        this.body = {error: String(e)};
      }
    } else {
      this.body = {error: "Please provide a valid API Token"};
    }
  })

  .put('/:id', verify(VERIFY, API_TOKEN), function *(next) {
    if(this.locals.valid) {
      try {
        // create query
        var query =  {_id:this.params.id}
        var fieldsToUpdate = this.request.body;
        var update = {$set: fieldsToUpdate}
        var data = yield data.findOneAndUpdate(
                                        query,
                                        update
                                      ).exec();
        this.body = {
          success: true,
          deleted: data
        };
      } catch (e) {
        this.body = {error: String(e)};
      }
    } else {
      this.body = {error: "Please provide a valid API Token"};
    }
  })


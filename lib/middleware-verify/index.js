/**
 * Export `verify`
 */

module.exports = verify;

/**
 * Initialize `protected`
 *
 * @return {Generator} next
 */

function verify(VERIFY, API_TOKEN) {
  return function *verify(next) {
    if(VERIFY) {
      if(this.query.token == API_TOKEN) {
        this.locals.valid = true;
      } else {
        this.locals.valid = false;
      }
    } else {
      this.locals.valid = true;
    }
    yield next;
  }
}

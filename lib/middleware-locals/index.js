/**
 * Middleware for locals to ensure that `ctx.locals`
 * is defined. Should be at the top of the middleware
 * stack.
 */

module.exports = function *(next) {
  this.locals = this.locals || {};
  this.locals.pretty = true;
  yield* next;
}


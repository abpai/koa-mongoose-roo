/**
 * Export `protected`
 */

module.exports = protected;

/**
 * Initialize `protected`
 *
 * @return {Generator} next
 */

function protected() {
  return function *protected(next) {

    // this.locals.user set by find-user-middleware up above
    if (this.session.authenticated) return yield next;

    // protected area, thou shall not pass.
    this.redirect('/login?redirect=' + encodeURIComponent(this.originalUrl));
  }
}

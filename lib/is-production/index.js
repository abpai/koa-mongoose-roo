/**
 * Are we running in production?
 */

module.exports = 'production' == process.env.NODE_ENV;

/**
 * Module Dependencies
 */

// var log = require('../loggers/log');

/**
 * Production
 */

var production = 'production' == process.env.NODE_ENV;

/**
 * Export 404
 */

module.exports = fourohfour;

/**
 * Handle errors in production
 */

function fourohfour() {
  return function *show404(next) {
    if (!production) return yield next;

    try {
      yield next;
      if (404 == this.response.status && !this.response.body) this.throw(404);
    } catch (err) {
      return this.redirect('/404');
    }
  }
}
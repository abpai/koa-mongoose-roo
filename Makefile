SERVERS := $(wildcard pages/**/index.js)
JS := $(filter-out $(SERVERS), $(wildcard pages/**/*.js))
CSS := $(wildcard pages/**/*.css)

server: node_modules
	@echo "[ SETTING SERVER CONFIGS ]"
	@./node_modules/.bin/prok \
		--env config/env.dev \
		--procfile config/Procfile.dev \
		--root .

build:
	@echo "[ RUNNING BUILD ]"
	@NODE_ENV=production DEBUG=duo:bundle ./node_modules/.bin/duo-bundle $(JS) $(CSS)

production: node_modules
	@echo "[ MAKING PRODUCTION ]"
	@NODE_ENV=production DEBUG=duo:bundle ./node_modules/.bin/duo-bundle $(JS) $(CSS)
	@NODE_ENV=production node --harmony index.js

node_modules: package.json
	@echo "[ DOWNLOADING NODE MODULES ]"
	@npm install
	@touch node_modules

clean:
	@echo "[ RUNNING CLEAN ]"
	@rm -rf node_modules components build

fake_clean:
	@echo "[ RUNNING CLEAN ]"
	@rm -rf components build

.PHONY: server build production clean fake_clean

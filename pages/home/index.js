"use strict";

/**
 * Module Dependencies
 */

var debug = require('debug')('krm:home')
var roo = module.exports = require('roo')(__dirname);
var production = require('../../lib/is-production');
var extend = require('extend.js');

/**
 * Duo
 */

!production && roo.duo('home.{css,js}');

/**
 * Initialize `roo`
 */

roo.get('/', function *(next) {
  var locals = extend(this.locals, this.params);
  this.body = yield roo.render('home.jade', locals);
});



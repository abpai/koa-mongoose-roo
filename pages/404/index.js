"use strict";

/**
 * Module Dependencies
 */

var debug = require('debug')('krm:404')
var roo = module.exports = require('roo')(__dirname);
var production = require('../../lib/is-production');
var extend = require('extend.js');

/**
 * Duo
 */

!production && roo.duo('404.{css,js}');



/**
 * Routing
 */

roo.all('/', function *(next) {
  console.log('request came in to 404');
  if (this.status != 404) return yield next;
  this.body = yield roo.render('404.jade', this.locals);
});

"use strict";

/**
 * Module Dependencies
 */

var debug = require('debug')('krm:ie');
var roo = module.exports = require('roo')(__dirname);
var production = require('../../lib/is-production');

/**
 * Duo
 */

!production && roo.duo('ie.{css,js}') && roo.duo('fonts*.css');


/**
 * Routing
 */

roo.all('/', function *(next) {
  this.body = yield roo.render('ie.jade', this.locals);
});
